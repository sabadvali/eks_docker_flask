FROM python:3.10.12

RUN mkdir /apps

WORKDIR /apps/

COPY . /apps/

RUN pip install -r requirements.txt

EXPOSE 80

CMD ["python", "app.py"]

