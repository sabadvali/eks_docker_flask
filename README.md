მარტივი python/flask აპლიკაცია რომელიც გვიბრუნებს პროცესორის და ოპერატიულის მოცულობას. და ასევე მიმდინარე დროს.
ეს ყველაფერი დაბილდულია docker ის საშვალებით. მიღებული image ატვირთულია Elastic Container Registry - ში.
deployment სერვისის საშვალებით ვქმნით კონტეინერს Elastic Kubernetes Service კლასტერში.
შემდგომ loadbalancer სერვისის საშვალებით ვხსნით წვდომას გაშვებულ კონტეინერზე.   

docker login

docker build -t image_name .

docker tag image_name:latest public_ecr_repository_name:latest

docker push public_ecr_repository_name:latest

kubectl apply -f deployment.yaml

kubectl apply -f loadbalancer.yaml
